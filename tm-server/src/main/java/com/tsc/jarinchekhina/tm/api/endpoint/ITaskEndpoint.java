package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Session;
import com.tsc.jarinchekhina.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskEndpoint extends IEndpoint<Task> {

    void clearTasks(@Nullable Session session);

    @NotNull
    List<TaskDTO> findAllTasks(@Nullable Session session);

    void createTask(@Nullable Session session, @Nullable String name);

    void createTaskWithDescription(@Nullable Session session, @Nullable String name, @Nullable String description);

    @NotNull
    TaskDTO findTaskById(@Nullable Session session, @Nullable String id);

    @NotNull
    TaskDTO findTaskByIndex(@Nullable Session session, @Nullable Integer index);

    @NotNull
    TaskDTO findTaskByName(@Nullable Session session, @Nullable String name);

    void removeTaskById(@Nullable Session session, @Nullable String id);

    void removeTaskByIndex(@Nullable Session session, @Nullable Integer index);

    void removeTaskByName(@Nullable Session session, @Nullable String name);

    void updateTaskById(
            @Nullable Session session,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    void updateTaskByIndex(
            @Nullable Session session,
            @Nullable Integer index,
            @Nullable String name,
            @Nullable String description
    );

    void startTaskById(@Nullable Session session, @Nullable String id);

    void startTaskByIndex(@Nullable Session session, @Nullable Integer index);

    void startTaskByName(@Nullable Session session, @Nullable String name);

    void finishTaskById(@Nullable Session session, @Nullable String id);

    void finishTaskByIndex(@Nullable Session session, @Nullable Integer index);

    void finishTaskByName(@Nullable Session session, @Nullable String name);

    void changeTaskStatusById(@Nullable Session session, @Nullable String id, @Nullable Status status);

    void changeTaskStatusByIndex(@Nullable Session session, @Nullable Integer index, @Nullable Status status);

    void changeTaskStatusByName(@Nullable Session session, @Nullable String name, @Nullable Status status);

    @NotNull
    List<TaskDTO> findAllTaskByProjectId(@Nullable Session session, @Nullable String projectId);

    @NotNull
    TaskDTO bindTaskByProjectId(@Nullable Session session, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    TaskDTO unbindTaskByProjectId(@Nullable Session session, @Nullable String taskId);

}
